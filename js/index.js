// Bài 1: Tính tiền lương nhân viên
function tinhLuong() {
  var luongNgayValue = document.getElementById("luongNgay").value;
  var soNgayValue = document.getElementById("soNgay").value;

  var tongLuong = document.getElementById("tongLuong");
  tongLuong.value = (luongNgayValue * soNgayValue).toLocaleString();
}
//Bài 2: Tính giá trị trung bình
function tinhTrungBinh() {
  var numberOneValue = document.getElementById("numberOne").value * 1;
  var numberTwoValue = document.getElementById("numberTwo").value * 1;
  var numberThreeValue = document.getElementById("numberThree").value * 1;
  var numberFourValue = document.getElementById("numberFour").value * 1;
  var numberFiveValue = document.getElementById("numberFive").value * 1;

  var giaTriTrungBinh = document.getElementById("giaTriTrungBinh");
  giaTriTrungBinh.value =
    (numberOneValue +
      numberTwoValue +
      numberThreeValue +
      numberFourValue +
      numberFiveValue) /
    5;
}

// Bài 3: Quy đổi tiền: USD => VNĐ
function quyDoiTien() {
  var numberUsdValue = document.getElementById("numberUsd").value;
  var priceUsdVnd = 23500;

  var quyDoiTien = document.getElementById("quyDoiTien");
  quyDoiTien.value = (numberUsdValue * priceUsdVnd).toLocaleString();
}

//Bài 4: Tính Diện tích và Chu vi của Hình chữ nhật
function tinhDtCv() {
  var chieuDaiValue = Number(document.getElementById("chieuDai").value);
  var chieuRongValue = Number(document.getElementById("chieuRong").value);

  var dienTich = document.getElementById("dienTich");
  var chuVi = document.getElementById("chuVi");

  // dienTich.value = new Intl.NumberFormat("vn-VN").format(chieuDaiValue * chieuRongValue);
  dienTich.value = (chieuDaiValue * chieuRongValue).toLocaleString();
  chuVi.value = ((chieuDaiValue + chieuRongValue) * 2).toLocaleString();
}

// Bài 5: Tính Tổng 2 ký số
function tinhTongKySo() {
  var numberValue = document.getElementById("number").value * 1;
  if (numberValue >= 0) {
    var hangChuc = Math.floor(numberValue / 10);
  } else {
    var hangChuc = Math.ceil(numberValue / 10);
  }
  var hangDonVi = numberValue % 10;

  var tinhTongKySo = document.getElementById("tinhTongKySo");
  tinhTongKySo.value = hangChuc + hangDonVi;
}
